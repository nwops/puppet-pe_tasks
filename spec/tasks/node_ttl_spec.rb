require 'bolt_spec/run'  
require 'spec_helper'
  describe 'node_ttl' do  
    let(:config_data) do
      {}
    end
    let(:inventory_data) do
      {
         
      }
    end

    include BoltSpec::Run  

    def bolt_config
      { 'modulepath' => RSpec.configuration.module_path }
    end

	  it 'should run a task on a node' do  
      result = run_task('pe_tasks::node_ttl', 'default', {})  
      expect(result[0]['status']).to eq('success')  
    end  
  end  