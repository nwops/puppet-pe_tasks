# Puppet Infrastructure tasks

Various tasks and plans for Puppet infrastucture 

#### Table of Contents

1. [Description](#description)
2. [Setup - The basics of getting started with pe_tasks](#setup)
    * [What pe_tasks affects](#what-pe_tasks-affects)
    * [Setup requirements](#setup-requirements)
    * [Beginning with pe_tasks](#beginning-with-pe_tasks)
3. [Usage - Configuration options and additional functionality](#usage)

## Description

This module is a gathering of various tasks that return information about your puppet infrastructure stack.
The tasks can be useful as a reporting mechanism or to further tie into future plans or automation calls.

The tasks are read only tasks that do not modify or call any commands that modify the OS.  They simply read information and return to the console.

## Setup

### Setup Requirements 

Current this is a bolt based module and requires bolt or PE orchestrator to run these tasks.

### Beginning with pe_tasks

## Usage

All of the tasks are meant to be part of a bigger system and therefor only return JSON formatted output.  It is assumed you will use this output in a downstream plan or other kind of automation.


## Tasks

* deactivated - list all nodes that have been deactived in puppetdb
* dead - nodes that have a signed certificate but are no longer present in puppetdb
* expired - list all nodes that have expired via the node_ttl in puppetdb
* infra_nodes - (experimental) -  list all nodes that make of the PE infrastructure stack
* node_ttl - Get the node ttl from puppetdb
* signed_nodes - Get a list of signed certs from the puppetserver CA.
* v3_functions - Get a json summary of v3 function usage from your puppet master environment.

## Examples

```
pdk bundle exec bolt task run pe_tasks::infra_nodes -t ssh://192.168.0.9:32783 --modulepath=spec/fixtures/modules --password=test
pdk (INFO): Using Ruby 2.5.7
pdk (INFO): Using Puppet 6.13.0
Started on ssh://192.168.0.9:32783...
Finished on ssh://192.168.0.9:32783:
  {
    "nodes": [
      "pe-xl-db-0.puppet.vm",
      "pe-xl-core-0.puppet.vm",
      "pe-xl-core-1.puppet.vm"
    ]
  }
Successful on 1 target: ssh://192.168.0.9:32783

pdk bundle exec bolt task run pe_tasks::node_ttl -t ssh://192.168.0.9:32783 --modulepath=spec/fixtures/modules --password=test
pdk (INFO): Using Ruby 2.5.7
pdk (INFO): Using Puppet 6.13.0
Started on ssh://192.168.0.9:32783...
Finished on ssh://192.168.0.9:32783:
  {
    "pe-xl-core-0.puppet.vm": "7d"
  }
Successful on 1 target: ssh://192.168.0.9:32783
Ran on 1 target in 3.66 sec
```

```shell
bolt task run pe_tasks::v3_functions -t master_host
[
  {
      "file": "/etc/puppetlabs/code/environments/production/modules/vagrant/manifests/package.pp",
      "line": "43",
      "content": "undef   => get_latest_vagrant_version(),",
      "function_module": {
        "path": "/etc/puppetlabs/code/environments/production/modules/vagrant/lib/puppet/parser/functions/get_latest_vagrant_version.rb",
        "module": "vagrant",
        "environment": "production",
        "name": "get_latest_vagrant_version"
      },
      "module": "vagrant"
  }
]

```