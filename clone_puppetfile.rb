#!/usr/bin/env ruby
# frozen_string_literal: true

# @author Corey Osman <corey@nwops.io>
# @description Uses a simple DSL similar to Puppetfile DSL to clone the puppet module
#  and push upto the github or github enterprise repo.
# @example Puppetfile
# github 'https://github.com', namespace: 'nwops', token: '123456'
# mod 'example42/psick'
# mod 'peadm', url: 'https://github.com/puppetlabs/puppetlabs-peadm'

require 'bundler/inline'

DEFAULT_BASE_URL = 'https://github.com'
DEFAULT_API_URL = 'https://api.github.com'
CLONE_CMD = 'git clone --bare'
PUSH_CMD = 'git push --mirror'
VERIFY_CMD = 'git ls-remote'
DEFAULT_DESCRIPTION = 'Clone from'

gemfile do
  source ENV['GEM_SOURCE'] || 'https://rubygems.org'
  gem 'puppet_forge'
  gem 'pry'
end

require 'json'
require 'puppet_forge'
require 'net/http'
require 'uri'
require 'openssl'
require 'puppet_forge/connection'

# DSL
# @param base_url defaults to github.com
# @option namespace - the namespace to create a repo in
# @option token - the github token, defaults to use environment variable
# @option api_url - the api url of github, defaults to api.github.com or adds /api to base_url
def github(base_url = DEFAULT_BASE_URL, namespace:, token:, proxy: nil, api_url: nil)
  @git_data ||= begin
    @base_url = base_url
    @api_url = DEFAULT_API_URL if @base_url.include?('github.com')

    @api_url ||= api_url || "#{base_url}/api"
    @namespace = namespace
    PuppetForge::Connection.proxy = proxy if proxy
    @token = token
    { api_url: @api_url, base_url: @base_url, namespace: namespace, token: @token }
  end
end

# DSL
# @param name [String] the name of the puppet forge module
# @option url [String] optionally supply the git url
def mod(name, url: nil)
  source_url = url || get_url(name)
  dir = File.basename(source_url)
  upload_url = "#{git_data[:base_url]}/#{git_data[:namespace]}/#{dir}"
  pulldown(source_url, dir)
  push(upload_url, dir, source_url)
  valid_repo?(upload_url)
end

private

def git_data
  @git_data || {}
end

# @param name [String] - the namespace/module_name of the forge module
# @return [String] - the source url of the forge module
def get_url(name)
  mod_name = name.gsub('/', '-')
  PuppetForge::Module.find(mod_name).attributes[:current_release][:metadata][:source]
end

# @param url [String] - the source url of the module to clone
# @return dir [String] - the directory to clone url to
def pulldown(url, dir)
  puts "Cloning #{url}"
  puts `#{CLONE_CMD} #{url} #{dir}` unless File.exist?(dir)
  dir
end

# @param name [String] the name of the repo to create in the github org
# @param source_url [String] - the git url of the source, used for description only
# @return [Boolean] - true if the repo was created
def create_repo(name, source_url)
  url = "#{git_data[:api_url]}/orgs/#{git_data[:namespace]}/repos"
  data = { name: name, description: "#{DEFAULT_DESCRIPTION} #{source_url}" }.to_json
  uri = URI.parse(url)
  request = Net::HTTP::Post.new(uri)
  request['Authorization'] = "token #{git_data[:token]}"
  request['Accept'] = 'application/vnd.github.v3+json'
  request.body = data

  req_options = {
    use_ssl: uri.scheme == 'https',
    verify_mode: OpenSSL::SSL::VERIFY_NONE
  }
  response = Net::HTTP.start(uri.hostname, uri.port, req_options) do |http|
    http.request(request)
  end
  success = response.code.to_i == 201
  raise response.message unless success

  success
end

# @param dir [String] the name of the dir where the repo was cloned to
# @param remote_url [String] - the git url of the new repo to push to
# @param source_url [String] - the git url of the source, used for description only
def push(remote_url, dir, source_url)
  Dir.chdir(dir) do
    create_repo(dir, source_url) unless valid_repo?(remote_url)
    `#{PUSH_CMD} #{remote_url}`
    $CHILD_STATUS.success?
  end
end

# @return [Boolean] - true if the remote repo exists
# @param url [String] - the remote repo url
def valid_repo?(url)
  puts "Verifying #{url} exists"
  uri = URI.parse(url)
  request = Net::HTTP::Head.new(uri)
  request['Authorization'] = "token #{git_data[:token]}"

  req_options = {
    use_ssl: uri.scheme == 'https',
    verify_mode: OpenSSL::SSL::VERIFY_NONE
  }

  response = Net::HTTP.start(uri.hostname, uri.port, req_options) do |http|
    http.request(request)
  end
  response.code.to_i == 200
end

# Eval the Puppetfile
unless ARGV.first && File.exist?(ARGV.first)
  puts "usage: #{__FILE__} <Puppetfile>"
  exit 1
end
eval(File.read(ARGV.first))
