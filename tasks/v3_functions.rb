#!/opt/puppetlabs/puppet/bin/ruby
# frozen_string_literal: true

if ENV['PT_module_environment']
  require 'puppet'
  begin
    Puppet.initialize_settings # required, otherwise grabbing the settings doesn't work
    lib = File.join(Puppet[:environmentpath], ENV['PT_module_environment'], 'modules', 'pe_tasks', 'files', 'pe_task_helper.rb')
    require lib
  rescue Puppet::DevError
    # do nothing
  end
else
  begin
    require_relative '../../pe_tasks/files/pe_task_helper.rb'
  rescue LoadError => e
    puts e.message
    exit 1
  end
end
require 'json'

include PeTaskHelper

# @param environment_name [String] - the environment name to use when searching
# @return [Has] - a hash of hashes that contain functions that are v3 functions
# @summary Walk the environment path and search for the older puppet 3.x functions in modules
#   this does not search for usage of those functions, only the functions themselves. 
# @example v3_functions('production') =>
# {
#   "network": {
#     "build_cidr_array": {
#       "path": "/etc/puppetlabs/code/environments/production/modules/network/lib/puppet/parser/functions/build_cidr_array.rb",
#       "module": "network",
#       "environment": "production",
#       "name": "build_cidr_array"
#     }
#   },
#   "psick": {
#     "parse_url": {
#       "path": "/etc/puppetlabs/code/environments/production/modules/psick/lib/puppet/parser/functions/parse_url.rb",
#       "module": "psick",
#       "environment": "production",
#       "name": "parse_url"
#     },
#     "get_magicvar": {
#       "path": "/etc/puppetlabs/code/environments/production/modules/psick/lib/puppet/parser/functions/get_magicvar.rb",
#       "module": "psick",
#       "environment": "production",
#       "name": "get_magicvar"
#     }
#   }
# }
def v3_functions(environment_name)
  environment_modules(environment_name).reduce({}) do |func_list, mod|
    mod_name, mod_path = mod
    v3_func_files = Dir.glob(File.join(mod_path, 'lib', 'puppet', 'parser', 'functions', '*'))
    next func_list if v3_func_files.empty?
    func_list[mod_name] = v3_func_files.reduce({}) do |acc, func_path|
      func_name = File.basename(func_path).sub('.rb', '')
      acc[func_name] = {'path' => func_path, 'module' => mod_name, 'environment' => environment_name, 'name' => func_name }
      acc
    end
    func_list
  end
end

# @param environment_name [String] - the environment name to use when searching
# @return [Hash] a hash of function name and values of where the function is found
# @example v3_function_table('production')
# {:build_cidr_array=>
#  {:path=>"/etc/puppetlabs/code/environments/production/modules/network/lib/puppet/parser/functions/build_cidr_array.rb",
#   :module=>"network",
#   :environment=>"production",
#   :name=>"build_cidr_array"},
# :parse_url=>
#  {:path=>"/etc/puppetlabs/code/environments/production/modules/psick/lib/puppet/parser/functions/parse_url.rb",
#   :module=>"psick",
#   :environment=>"production",
#   :name=>"parse_url"},
# :get_magicvar=>
#  {:path=>"/etc/puppetlabs/code/environments/production/modules/psick/lib/puppet/parser/functions/get_magicvar.rb",
#   :module=>"psick",
#   :environment=>"production",
#   :name=>"get_magicvar"}}
def v3_function_table(environment_name)
  @v3_function_table ||= v3_functions(environment_name).reduce({}) {|acc,d| acc.merge(d.last) }
end

# @return [Array] a list of module that contain v3 functions
# @param environment_name [String] - the environment name to use when searching
def mods_with_v3_functions(environment_name)
  v3_functions(environment_name).keys
end

# @param environment_name [String] - the environment name to use when searching
# @return [Array] an array of hashes thst contain where the function is being used
# @summary Find all usages of deprecated or v3 functions in the codebase
# @example v3_function_usages('production') => 
# [
#   {
#   "file": "/etc/puppetlabs/code/environments/production/modules/tp/manifests/uninstall.pp",
#   "line": "43",
#   "content": "$tp_settings=tp_lookup($title,'settings',$data_module,'merge')",
#   "from_module": {
#     "path": "/etc/puppetlabs/code/environments/production/modules/tp/lib/puppet/parser/functions/tp_lookup.rb",
#     "module": "tp",
#     "environment": "production",
#     "name": "tp_lookup"
#   },
#   "module": "tp"
#   }
# ]
def v3_function_usages(environment_name = 'production')
  mod_functions = v3_functions(environment_name)
  core_v3_funcs = deprecated_v3_functions.keys
  mod_func_table = mod_functions.reduce(deprecated_v3_functions) {|acc,d| acc.merge(d.last) }
  file_pattern = File.join(environments[environment_name], '**', '*', '**', '*.pp')
  # create a regex with | operators on every func name
  functions = mod_func_table.keys.map {|f| f + '\(' }.join('|')
  data = `egrep -Hn -e "#{functions}" #{file_pattern} 2>&1`
  if $?.success?  
    data.split("\n").map do | line |
      matches = line.match(/^(?<file>.*\.pp)\:(?<line>\d+)\:(?<content>.*)/)
      if matches
        entry = matches.named_captures
        # we don't care about test files
        next if entry["file"] =~ /.*\/tests|examples\/*.*.pp/

        # get the name of the function we found being used
        found_function_match = entry["content"].match(/(\w+)\(/)
        entry['function_module'] = mod_func_table[found_function_match[1]] if found_function_match
        entry["content"] = entry["content"].strip
        entry["module"] = File.basename(File.dirname(File.dirname(entry["file"])))
        entry
      end
    end.compact
  end
end

puts "Below is a list of puppet v3 function usage we found in the #{ENV['PT_environment_name']} environment."
puts "Usage of v3 functions is discourged and should be updated to v4 or native functions if applicable."
puts "In many cases the v3 function source code will likely suggest an alternative method.\n"
puts "This report should only be seen as a warning."

puts JSON.pretty_generate(v3_function_usages(ENV['PT_environment_name']))
