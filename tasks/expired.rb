#!/opt/puppetlabs/puppet/bin/ruby
# Puppet Task Name: deactivated

if ENV['PT_module_environment']
  require 'puppet'
  begin
    Puppet.initialize_settings # required, otherwise grabbing the settings doesn't work
    lib = File.join(Puppet[:environmentpath], ENV['PT_module_environment'], 'modules', 'pe_tasks', 'files', 'pe_task_helper.rb')
    require lib
  rescue Puppet::DevError
    # do nothing
  end
else
  begin
    require_relative '../../pe_tasks/files/pe_task_helper.rb'
  rescue LoadError => e
    puts e.message
    exit 1
  end
end
include PeTaskHelper

data = {}
data['expired'] = expired_nodes
puts data.to_json