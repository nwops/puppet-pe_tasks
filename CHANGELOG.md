# Changelog
## Release 0.1.1
* Ensure PE 2018 can load dependent files for tasks

## Release 0.1.0
Initial release
