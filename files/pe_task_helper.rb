require 'json'
require 'puppet'
require 'puppet/face'

# this is a helper file for many of the bolt tasks in this module

module PeTaskHelper
  begin
    Puppet.initialize_settings # required, otherwise grabbing the settings doesn't work
  rescue Puppet::DevError
    # do nothing
  end
  CA_DIR = Puppet.settings[:cadir]
  PE_LIB = '/opt/puppetlabs/puppet/modules/puppet_enterprise/lib'
  $LOAD_PATH << PE_LIB unless $LOAD_PATH.include?(PE_LIB)
  NODE_FACE = Puppet::Face[:node, :current]
  SIGNED_DIR = File.join(CA_DIR, 'signed')
  FUNCTION_WHITELIST = %w(require.rb include.rb contain.rb)

  # @return [Hash] hash of certnames
  def deactivated_nodes
    @deactivated_nodes ||= begin
      node_query("nodes[certname] {node_state = 'inactive' and deactivated is not null }")
    end
  end

  # @return [Boolean] true if the node is a ca
  def is_a_ca?
    if Puppet::Interface.find_action(:node, :deactivate, :current)
    true
    else
      raise 'This command cannot be run on an agent node. Please try again on a master node.'
    end
  end

  # @summary 
  #   searches through the puppet code base and finds older v3 functions that have
  #   the word deprecated in it.  While this is not a fool proof way to find these things
  #   it works for the time being.
  def deprecated_v3_functions
    dir = File.join(puppet_dir,'puppet', 'parser', 'functions')
    # search for the word deprecated in the function file
    # TODO find native ruby for grep
    funcs = `grep -rl 'deprecated' #{dir}`.split("\n")
    list = funcs.reject do |func_file| 
      FUNCTION_WHITELIST.include?(File.basename(func_file)) 
    end
    list.reduce({}) do |acc, path| 
      name = File.basename(path).sub('.rb', '')
      acc[name] = path
      acc
    end
  end

  # @return [Array] - return an array of file paths that are v3 functions in puppet gem
  def puppet_v3_functions
    Dir.glob(File.join(puppet_dir,'puppet', 'parser', 'functions', '*'))
  end
  
  # @return [String] the path of where the puppet gem is located
  def puppet_dir
    File.dirname Puppet.method(:settings).source_location.first
  end

  # @return [Hash] hash of certnames
  def expired_nodes
    @expired_nodes ||= node_query("nodes[certname] {node_state = 'inactive' and expired is not null }")
  end

  # @return [String] - hopefully a json formatted string
  def puppetdb_query(query)
    out = "curl -s -X GET https://#{Puppet.settings[:server]}:8081/pdb/query/v4 \
    --tlsv1 \
    --cacert #{Puppet.settings[:cacert]} \
    --cert #{Puppet.settings[:hostcert]} \
    --key #{Puppet.settings[:hostprivkey]} \
    --data-urlencode \"query=#{query}\""
    data = `#{out}`
    begin
      JSON.parse(data)
    rescue JSON::ParserError
      puts data
      raise "Invalid json returned"
    end
  end

  # @return [Hash] - a hash of environment name with paths
  # @example environments => {'production' => '/etc/puppetlabs/code/environments/production'}
  def environments(environment_path = Puppet[:environmentpath] )
    Dir.glob(File.join(environment_path, '*')).reduce({}) do |acc, dir|
      name = File.basename(dir)
      acc[name] = dir
      acc
    end
  end

  # @return [Hash] - a hash of module name with paths
  # @example environment_modules(production) => {"deployments"=>"/etc/puppetlabs/code/environments/production/modules/deployments"}
  def environment_modules(environment)
    Dir.glob(File.join(environments[environment], 'modules', '*')).reduce({}) do |acc, dir|
      name = File.basename(dir)
      acc[name] = dir
      acc
    end
  end

  # @return [Array] returns all the puppet files in the module
  # @param environment_name [String] the environment name
  # @param mod_name [String] the name of the module you wish to search
  def pp_manifests(environment_name, mod_name = '**')
    Dir.glob(File.join(environments[environment_name], 'modules', mod_name, '**', '*.pp'))
  end

  # @return [Hash] - a list of node certnames as a hash
  def node_query(query)
    data = puppetdb_query(query)
    data.each_with_object({}) do |node, list|
      list[node['certname']] = node['value']
    end
  end

  # @return [Hash] - a certname to hostname Hash map of all the nodes
  # @note This is an expensive call on large infrastructures
  def cert_host_map
    @cert_host_map ||= node_query("facts {name = 'fqdn' }")
  end

  # @return [Array] list of signed cert names
  # @example
  #  signed_certs => ['pe-xl-core0.puppet.vm.pem']
  def signed_certs
    @signed_certs ||= Dir.glob(File.join(SIGNED_DIR, '*.pem'))
  end

  # @return [Array] list of signed cert names for nodes
  # @example
  #  signed_certs => ['pe-xl-core0.puppet.vm.pem']
  def node_certs
    @node_certs ||= signed_certs.reject do |cert|
      protected_certs.include?(cert)
    end
  end

  # @return [Array] - an array of hostnames that are participating in the pe infrastructure
  def infra_nodes
    data = puppetdb_query("resources[parameters] { type = 'Class' and title = 'Puppet_enterprise'}")
    params = data[0]['parameters']
    params.fetch_values('puppetdb_database_host', 'certificate_authority_host', 'console_database_host',
                        'console_host', 'database_host', 'puppetdb_host', 'puppet_master_host',
                        'pcp_broker_host', 'orchestrator_database_host',
                        'inventory_database_host', 'ha_enabled_replicas').flatten.uniq
  end

  # @return [Array] - a list of special certs that should be protected
  def protected_certs
    @protected_certs ||= ['console-cert'] + infra_nodes
  end

  # @return [String] - the value of the node ttl
  # @note calls out to puppetdb and checks the node_ttl from the class resource
  def node_ttl
    query = "resources[parameters] {type = 'Class' and title = 'Puppet_enterprise::Profile::Puppetdb' }"
    data = puppetdb_query(query)
    data.first['parameters']['node_ttl']
  end

  # @return [Hash] a hash of all the hosts where the keys are the hostname
  #   removes the .pem from the signed_cert filename
  # @example
  #   signed_hosts => { "hostname12345.vm" => { :hostname => "hostname12345.vm"}}
  def signed_hosts
    @signed_hosts ||= begin
      signed_certs.map do |file|
        File.basename(file, File.extname(file))
      end
    end
  end

  # @return [Array] - an array of all puppetdb nodes identified by the certname
  def puppetdb_nodes
    @puppetdb_nodes ||= puppetdb_query("nodes[certname] {node_state != 'any'}").map {|n| n['certname'] }
  end

  # @return [Array] - nodes that have a signed certificate but are no longer present in puppetdb
  # This is determined by getting all nodes in puppetdb and taking the difference from the certs available on
  # the CA.
  # @note There still may be room for error if certs are used for non puppet-agent things.
  def dead_nodes
    signed_hosts - (puppetdb_nodes + protected_certs)
  end

  # @return [String] - the certname of the localhost
  def local_certname
    Puppet.settings[:certname]
  end
end
